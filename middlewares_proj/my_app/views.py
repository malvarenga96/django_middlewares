import logging

from rest_framework.views import APIView
from rest_framework.response import Response


logger = logging.getLogger(__name__)


class HelloWorldAPIView(APIView):

    def get(self, request):
        logger.info('You are here! At hello-word endpoint')
        return Response(
            data={'hello': 'Hello World'},
            status=200,
        )
