# Django Middleware example

## Installation

- Copy `.env.example` to `.env` file
- Build the project: `docker-compose build`
- Start up the database server: `docker-compose up -d db`
- Create database: `docker-compose run web python manage.py migrate`
- Start up web server: `docker-compose up -d web`
- Navigate through the different branches
