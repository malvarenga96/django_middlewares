import setuptools


setuptools.setup(
    name='middlewares',
    version='0.0.0',
    author='Marvin Alvarenga',
    author_email='malvarenga@applaudostudios.com',
    description='A simple example about creating middlewares at Django',
    packages=setuptools.find_packages(),
    scrips=['manage.py'],
)
